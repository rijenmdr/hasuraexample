import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from 'react-redux';
import  { BrowserRouter } from 'react-router-dom';
import {ApolloClient} from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {HttpLink} from 'apollo-link-http';
import {ApolloProvider} from '@apollo/react-hooks'
import store from './redux/store';

const cache=new InMemoryCache();
const link =new HttpLink({
  uri:`https://todoapplication.hasura.app/v1/graphql`
})

const client=new ApolloClient({
  cache,
  link
})

const app=(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <BrowserRouter>
        <App/>
        </BrowserRouter>
    </Provider>
  </ApolloProvider>
)
ReactDOM.render(
    app,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
