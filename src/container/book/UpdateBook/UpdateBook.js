import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useParams } from 'react-router-dom';
import {fetchBookById,updateBook} from '../services/bookAction';

const UpdateBook=()=> {
    const {bookId}=useParams();
    const bookList = useSelector(state => state.bookList)
    const [name,setName]=useState("");
    const [price,setPrice]=useState();

    const dispatch=useDispatch();


    useEffect(() => {
       dispatch(fetchBookById(bookId));
       setName(bookList.book.book_name)
       setPrice(bookList.book.price);
    }, [])

    const updateBookHandler=(id,name,price)=>{
        dispatch(updateBook({}))
    }

   
    return (
        <div>
            <div>
            <label htmlFor="Book Name">BookName</label>
            <input type="text" name="bookName" value={name} onChange={(e)=>setName(e.target.value)}/>
            </div>
            <div>
            <label htmlFor="Price">Price</label>
            <input type="number" name="price" value={price} onChange={(e)=>setPrice(e.target.value)}/>
            </div>
            <button onClick={()=>updateBookHandler(bookList.book.id,name,price)}>Update</button>
        </div>
    )
}



export default UpdateBook
