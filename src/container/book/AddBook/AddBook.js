import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, useHistory } from 'react-router-dom';
import {addBook} from '../services/bookAction';

const AddBook=()=>{
    const [bookName,setBookName]=useState('');
    const [price,setPrice]=useState();
    const bookList = useSelector(state => state.bookList)
    console.log(bookList.insertionObj)
   

    const dispatch=useDispatch();

    const history=useHistory()

    // useEffect(() => {
    //     history.push('/')
    // }, [bookList.message])

    const submitBookInfo=()=>{
        dispatch(addBook(bookName,price));
        history.push('/')
    }

    return (
        
        <div>
            <div>
            <label htmlFor="Book Name">BookName</label>
            <input type="text" name="bookName" value={bookName} onChange={(e)=>setBookName(e.target.value)}/>
            </div>
            <div>
            <label htmlFor="Price">Price</label>
            <input type="number" name="price" value={price} onChange={(e)=>setPrice(e.target.value)}/>
            </div>
            <button onClick={submitBookInfo}>Submit</button>
        </div>
        
    )
}

export default AddBook
