import React, { useEffect, useState } from 'react';
import {connect, useDispatch, useSelector} from 'react-redux';
import * as bookAction from './services/bookAction';
import './Book.css';
import { useHistory } from 'react-router-dom';

const Book=(props)=> {
    // const{
    //     bookList,
    //     getBooks
    // }=props
    const bookList = useSelector(state => state.bookList)
    console.log(bookList.books)
    const dispatch=useDispatch();
    const history=useHistory();
    
    useEffect(() => {
        dispatch(bookAction.fetchBooks());
    }, [bookList.deleteMessage])

    const deleteHandler=(bookId)=>{
        console.log(bookId)
        dispatch(bookAction.deleteBook(bookId));
    }

    const updateHandler=(bookId)=>{
        history.push('/update-book/'+bookId);
    }
    return (
        <div className="books">
            {
                bookList.books.map((book,key)=>(
                    <div className="book" key={key}>
                        <div className="book-image">
                            <img className="image" src="https://media.gettyimages.com/photos/stack-of-books-picture-id157482029?s=612x612"/>
                        </div>
                        <div className="book-name">
                            {book.book_name}
                        </div>
                        <div className="book-price">
                           Price: {book.price}
                        </div>
                        {/* <div className="book-published">
                            {v.published_at}
                            </div> */}
                            <div className="book-author">
                                Author:{book.author.name}
                                </div>
                            <button onClick={()=>updateHandler(book.id)}>Update</button>
                            <button onClick={()=>deleteHandler(book.id)}>Delete</button>
                    </div>
                ))
            }
        </div>
    )
}

// const mapStateToProps=(state)=>{
//     return{
//         bookList:state.bookReducer.books
//     }
// }

// const mapDispatchToProps=(dispatch)=>{
//     return {
//         getBooks:()=>dispatch(bookAction.fetchBooks())
//     }
// }

 export default Book
