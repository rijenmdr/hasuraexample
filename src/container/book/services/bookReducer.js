import action from './bookAction';
import * as bookConstant from './bookConstant';

const initialState={
    books:[],
    insertionObj:{},
    deleteMessage:"",
    updateMessage:"",
    book:{}
}

function bookReducer (state=initialState,action){
    switch(action.type){
        case "GET_BOOKS":
            return{
                ...state,
                books:action.books
            }
            case "POST_BOOKS":
                return {
                    ...state,
                    insertionObj:{...action.insertionObj}
                }
            case "DELETE_BOOK":
                return{
                    ...state,
                    deleteMessage:action.deleteMessage
                }
            case "UPDATE_BOOK":
                return{
                    ...state,
                    updateMessage:action.updateMessage
                }
            case "GET_SINGLE_BOOK":
                return{
                    ...state,
                    book:action.book
                }
        default:
            return state
    }
}

export {bookReducer}