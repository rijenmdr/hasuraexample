import gql from 'graphql-tag';
import * as actionType from './bookConstant';
import graphQlClient from '../../../redux/apolloclient';

export const fetchSuccess=(books)=>{
    console.log(books)
    return {
        type:"GET_BOOKS",
        books:books
    }
}

export const bookUploader=(insertionObj)=>{
    return {
        type:"POST_BOOK",
        insertionObj:insertionObj
    }
}

export const fetchBookSuccess=(book)=>{
    console.log(book[0]);
    return {
        type:"GET_SINGLE_BOOK",
        book:book[0]
    }
}

export const deleteSuccess=(message)=>{
    return {
        type:"DELETE_BOOK",
        deleteMessage:message
    }
}

export const updateSuccess=(message)=>{
    return {
        type:"UPDATE_BOOK",
        updateMessage:message
    }
}

export const fetchBooks=()=>(dispatch)=>{
    graphQlClient
    .mutate({
        mutation:gql`
        query MyQuery {
            book {
              id
              book_name
              price
              published_at
              author {
                name
              }
            }
          }        
        `,
        update:(_cache,result)=>{
            console.log(_cache)
            dispatch(fetchSuccess(result.data.book))
        },
    })
    .catch(error=>{
        console.log(error);
    })
}

export const fetchBookById=(bookId)=>(dispatch)=>{
    graphQlClient
    .mutate({
        variables:{bookId:bookId},
        mutation:gql`
        query MyQuery($bookId:Int) {
            book(where: {id: {_eq: $bookId}}){
              id
              book_name
              price
              author_id
            }
          }
        `
        ,update:(_cache,result)=>{
            dispatch(fetchBookSuccess(result.data.book))
        }
    })
}

export const addBook=(bookName,price)=>(dispatch)=>{
    graphQlClient
    .mutate({
        variables:{bookName:bookName,price:price},
        mutation:gql`
        mutation MyMutation($bookName:String,$price:numeric) {
            insert_book(objects: {book_name:$bookName , price:$price, author_id: 2}){
              returning{
                book_name
              }
            }
          }
        `
        ,update:(_cache,result)=>{
            dispatch(bookUploader({
                message:"Insertion Successfull",
                isInserted:true
            }))
        }
    })
    .catch(error=>{
        console.log(error);
    })
}

export const deleteBook=(bookId)=>(dispatch)=>{
    console.log(bookId)
    graphQlClient
    .mutate({
        variables:{bookId:bookId},
        mutation:gql`
        mutation MyMutation ($bookId:Int){
            delete_book(where: {id: {_eq: $bookId}}){
              returning{
                book_name
              }
            }
          }          
        `,
        update:(_cache,result)=>{
            dispatch(deleteSuccess("Deletion Successful"));
        }
    })
    .catch(error=>{
        console.log(error);
    })
}

export const updateBook=(book)=>(dispatch)=>{
    graphQlClient
    .mutate({
        variables:{bookId:book.id,bookName:book.name,price:book.price},
        mutation:gql`
        mutation MyMutation($bookId:Int,$bookName:String,$price:numeric) {
            update_book(where: {id: {_eq: $bookId}}, _set: {book_name: $bookName, price:$price}){
              returning{
                book_name
              }
            }
          }
        `,
        update:(_cache,result)=>{
            dispatch(updateSuccess("Update successful"))
        }
    })
}