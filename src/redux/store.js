import {createStore,applyMiddleware, combineReducers} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {bookReducer} from '../container/book/services/bookReducer';
// import rootReducer from './reducer'

const middleware=[thunk];
const rootReducer=combineReducers({
    bookList:bookReducer
})

const store=createStore(
    rootReducer,composeWithDevTools(applyMiddleware(...middleware))
)

export default store;