import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";

//cache variable that hold cache values.
const cache = new InMemoryCache();

const link = new HttpLink({
    //graphql api uri
    uri:`https://todoapplication.hasura.app/v1/graphql`
});

const client = new ApolloClient({
    cache,
    link
});

export default client;
