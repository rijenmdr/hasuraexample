import { combineReducers } from 'redux';
import {bookReducer} from '../../container/book/services/bookReducer';

export default combineReducers({
    bookReducer:bookReducer
})