import logo from './logo.svg';
import './App.css';
import Book from './container/book/Book';
import { Link, Route, Switch } from 'react-router-dom';
import AddBook from './container/book/AddBook/AddBook';
import UpdateBook from './container/book/UpdateBook/UpdateBook';

function App() {
  return (
    <div>
      <div className="nav-bar">
        <Link to="/" >Books</Link>
        <Link to="/add-books">Add-Book</Link>
      </div>
      <Switch>
        <Route path="/update-book/:bookId" component={UpdateBook}/>
        <Route path='/add-books' component={AddBook}/>
        <Route path="/" exact component={Book}/>
        <Route path="/books" exact component={Book}/>
      </Switch>
    </div>
  );
}

export default App;
